
var canvas, ctx;

// wall
var wall = {
	left: 0,
	right: 0,
	top: 0,
	bottom: 0
}

// circle
var circle = {
	x: 0,
	y: 0,
	radius: 10
}

var velX = 5; // x velocity
var velY = 2; // y velocity

// Smart animations
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

// Draw the ball
function drawBall( ) {

	  ctx.beginPath( );
      ctx.arc( circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false );
      ctx.fillStyle = 'red';
      ctx.fill( );

      ctx.lineWidth = 2;
      ctx.strokeStyle = '#111111';
      ctx.stroke( );
}

function execute( ) {

	ctx.clearRect( wall.left, wall.top, wall.right, wall.bottom ); // clear the canvas

	circle.x += velX;
	circle.y += velY;

	/* This is where your code will go */



	/* This is where your code will end */

	drawBall( ); // draw the ball
}

// Initialize variables 
(function init( ) {

	canvas 		= document.getElementById("bounce");
	ctx 		= canvas.getContext("2d");

	wall.right 	= canvas.width;
	wall.bottom	= canvas.height;

	// notice we do not start circle at (0,0)
	circle.x 	= circle.radius; 
	circle.y 	= circle.radius; 

})();

// The loop!
(function run( ) {

  execute( );

  requestAnimFrame( run );
})();
